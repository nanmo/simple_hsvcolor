/*

Copyright (c) 2012, nanmo (@nanimosa)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
- Neither the name of the "nanmo" nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

using UnityEngine;
using System.Collections;

public class HSVColor : MonoBehaviour {
	
	//reference http://ja.wikipedia.org/wiki/HSV%E8%89%B2%E7%A9%BA%E9%96%93
	public static Color GetRGBColorFromHSV( float hue, float saturation, float val ) {
		if ( saturation <= 0.0f ) {
			return new Color( val, val, val );
		}
		
		int h = ( (int)hue / 60 ) % 6;
		float f = hue / 60 - h;
		float p = val * ( 1 - saturation );
		float q = val * ( 1 - f * saturation);
		float t = val * ( 1 - ( 1 - f ) * saturation );
		
		if  ( h == 0 ) {
			return new Color( val, t, p );
		}
		else if ( h == 1 ) {
			return new Color( q, val, p );
		}
		else if ( h == 2 ) {
			return new Color( p, val, t );
		}
		else if ( h == 3 ) {
			return new Color( p, q, val );
		}
		else if ( h == 4 ) {
			return new Color( t, p, val );
		}
		else if ( h == 5 ) {
			return new Color( val, p, q );
		}
		return Color.white;
	}
}
